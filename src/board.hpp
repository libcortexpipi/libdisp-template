#pragma once

#include <cortexpipi/helpers/singleton.hpp>
#include <cortexpipi/gpio.hpp>


class Board: public cortexpipi::helpers::Singleton<Board> {
public:
  Board();
  using GpioPin = cortexpipi::GpioPin;
  GpioPin led;
  GpioPin btn;
};
