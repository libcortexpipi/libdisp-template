
#include "board.hpp"
#include <cortexpipi/systick.hpp>
#include <libdisp/DisplayInterfaceSPI.hpp>



int main() {
  auto& systick = cortexpipi::Systick::instance();
  auto& board = Board::instance();
  disp::DisplayInterfaceSPI di(3, {0,0}, {0,1}, {0,2});
  for(;;) {
    board.led.toggle();
    systick.delay(board.btn.read()?100:200);
  }

  return 0;
}
