
message(STATUS "adding libopencm3")

set(LIBCORTEXPIPI_DIR ${PROJECT_SOURCE_DIR}/libcortexpipi)


function(JOIN VALUES GLUE OUTPUT)

    string(REGEX REPLACE "([^\\]|^);" "\\1${GLUE}" _TMP_STR "${VALUES}")

    string(REGEX REPLACE "[\\](.)" "\\1" _TMP_STR "${_TMP_STR}") #fixes escaping

    set(${OUTPUT} "${_TMP_STR}" PARENT_SCOPE)

endfunction()

# Generate linker information for device, based on libopencm3/mk/genlink-config.mk
if (NOT DEVICE)
    message(FATAL_ERROR "No DEVICE specified for linker script generator")
endif ()


find_program(PYTHON python)
if (NOT PYTHON)
    message(FATAL_ERROR "python is required to generate the linker script, please install it.")
endif ()

set(GENLINK_SCRIPT "${LIBCORTEXPIPI_DIR}/scripts/genlink.py")
set(DEVICES_DATA "${LIBCORTEXPIPI_DIR}/ld/devices.data")
execute_process(
        COMMAND "${PYTHON}" "${GENLINK_SCRIPT}" "${DEVICES_DATA}" "${DEVICE}" "FAMILY"
        OUTPUT_VARIABLE GENLINK_FAMILY
)
execute_process(
        COMMAND "${PYTHON}" "${GENLINK_SCRIPT}" "${DEVICES_DATA}" "${DEVICE}" "USBFAMILY"
        OUTPUT_VARIABLE GENLINK_SUBFAMILY
)
execute_process(
        COMMAND "${PYTHON}" "${GENLINK_SCRIPT}" "${DEVICES_DATA}" "${DEVICE}" "CPU"
        OUTPUT_VARIABLE GENLINK_CPU
)
execute_process(
        COMMAND "${PYTHON}" "${GENLINK_SCRIPT}" "${DEVICES_DATA}" "${DEVICE}" "FPU"
        OUTPUT_VARIABLE GENLINK_FPU
)
execute_process(
        COMMAND "${PYTHON}" "${GENLINK_SCRIPT}" "${DEVICES_DATA}" "${DEVICE}" "CPPFLAGS"
        OUTPUT_VARIABLE GENLINK_CPPFLAGS
)
execute_process(
        COMMAND "${PYTHON}" "${GENLINK_SCRIPT}" "${DEVICES_DATA}" "${DEVICE}" "DEFS"
        OUTPUT_VARIABLE GENLINK_DEFS
)

set(LINKER_SCRIPT "generated.${DEVICE}.ld")
set(LINKER_SCRIPT_PATH "${CMAKE_CURRENT_BINARY_DIR}/${LINKER_SCRIPT}")
list(APPEND ARCH_FLAGS -mcpu=${GENLINK_CPU})
list(APPEND ARCH_FLAGS -mthumb)
# Check FPU

if (GENLINK_FPU STREQUAL "soft")
    list(APPEND ARCH_FLAGS -msoft-float)
elseif (GENLINK_FPU STREQUAL "hard-fpv4-sp-d16")
    list(APPEND ARCH_FLAGS -mfloat-abi=hard -mfpu=fpv4-sp-d16)
elseif (GENLINK_FPU STREQUAL "hard-fpv5-sp-d16")
    list(APPEND ARCH_FLAGS -mfloat-abi=hard -mfpu=fpv5-sp-d1)
else ()
    message(WARNING "No match for the FPU flags")
endif ()

string(TOUPPER ${GENLINK_FAMILY} ARCH)
list(APPEND ARCH_FLAGS ${${ARCH}_CFLAGS})

string(REPLACE " " ";" GENLINK_DEFS ${GENLINK_DEFS})
string(REPLACE " " ";" GENLINK_CPPFLAGS ${GENLINK_CPPFLAGS})


execute_process(
        COMMAND ${CMAKE_CXX_COMPILER} ${ARCH_FLAGS} ${GENLINK_DEFS} "-P" "-E" "${LIBCORTEXPIPI_DIR}/ld/linker.ld.S"
        OUTPUT_FILE "${CMAKE_CURRENT_BINARY_DIR}/${LINKER_SCRIPT}"
)




message(STATUS "\tCPU: ${GENLINK_CPU}")
message(STATUS "\tFamily: ${GENLINK_FAMILY}")
message(STATUS "\tSubfamily: ${GENLINK_SUBFAMILY}")
message(STATUS "\tDefines: ${GENLINK_DEFS}")
message(STATUS "\tCPP flags: ${GENLINK_CPPFLAGS};${ARCH_FLAGS}")
message(STATUS "\tFPU: ${GENLINK_FPU}")

set(LIBCORTEXPIPI_LIBS cortexpipi-${GENLINK_FAMILY})
set(LIBCORTEXPIPI_INCLUDE ${LIBCORTEXPIPI_DIR}/include)
message(STATUS "\tAlso CPP flags(${ARCH}_CFLAGS): ${${ARCH}_CFLAGS}")
set(LIBCORTEXPIPI_COMPILE_FLAGS ${GENLINK_CPPFLAGS} ${ARCH_FLAGS} )
set(
  LIBCORTEXPIPI_LINK_FLAGS
  -T ${LINKER_SCRIPT_PATH}
  ${ARCH_FLAGS}
  -Wl,--start-group
  -lc -lgcc -lnosys
  -Wl,--end-group
  -nostartfiles
)

add_link_options(-T ${LINKER_SCRIPT_PATH} ${ARCH_FLAGS} -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group -nostartfiles)
