#pragma once

#include <cstddef>
#include <cstdint>

namespace disp {

class DisplayInterface {
public:

    virtual void writeCommand8(uint8_t cmd) = 0;
    virtual void writeData8(uint8_t data) = 0;
    virtual void writeReg8(uint8_t reg, uint8_t data) = 0;

    virtual void writeCommand16(uint16_t cmd) = 0;
    virtual void writeData16(uint16_t data) = 0;
    virtual void writeReg16(uint8_t reg, uint16_t data) = 0;

    virtual void writeData(uint8_t * data, size_t len) = 0;

    virtual ~DisplayInterface(){};
};
}
