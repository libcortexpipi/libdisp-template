#pragma once


#include <libdisp/DisplayInterface.hpp>
#include <cortexpipi/gpio.hpp>
#include <cortexpipi/spi.hpp>


namespace disp {


class DisplayInterfaceSPI: public DisplayInterface {
public:
    using SPI = cortexpipi::SPI;
    using GpioPin = cortexpipi::GpioPin;


    DisplayInterfaceSPI(uint8_t spi, const GpioPin& dc, const GpioPin& cs, const GpioPin& res);
    virtual ~DisplayInterfaceSPI();

    virtual void writeCommand8(uint8_t cmd);
    virtual void writeData8(uint8_t data);
    virtual void writeReg8(uint8_t reg, uint8_t data);

    virtual void writeCommand16(uint16_t cmd);
    virtual void writeData16(uint16_t data);
    virtual void writeReg16(uint8_t reg, uint16_t data);

    virtual void writeData(uint8_t * data, size_t len);
private:
    const GpioPin _dc;
    const GpioPin _cs;
    const GpioPin _res;
    const SPI _spi;

};
}
