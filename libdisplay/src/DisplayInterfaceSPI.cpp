#include <libdisp/DisplayInterfaceSPI.hpp>


namespace disp{

DisplayInterfaceSPI::DisplayInterfaceSPI (
    uint8_t spi,
    const GpioPin& dc,
    const GpioPin& cs,
    const GpioPin& res
):
    _dc(dc),
    _cs(cs),
    _res(res),
    _spi(spi)
{

}

DisplayInterfaceSPI::~DisplayInterfaceSPI() {

}

void DisplayInterfaceSPI::writeCommand8(uint8_t cmd) {

}
void DisplayInterfaceSPI::writeData8(uint8_t data) {

}
void DisplayInterfaceSPI::writeReg8(uint8_t reg, uint8_t data) {

}

void DisplayInterfaceSPI::writeCommand16(uint16_t cmd) {

}
void DisplayInterfaceSPI::writeData16(uint16_t data) {

}
void DisplayInterfaceSPI::writeReg16(uint8_t reg, uint16_t data) {

}

void DisplayInterfaceSPI::writeData(uint8_t * data, size_t len) {

}

}
